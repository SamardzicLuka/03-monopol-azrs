#ifndef CARD_CHANCE_COMMUNITY_HPP
#define CARD_CHANCE_COMMUNITY_HPP

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

#include <QPainter>
#include <QString>
#include <QGraphicsItem>
enum class Card_category
{
    Chance,
    Community
};
enum class Card_type
{
   Move,
   Get,
   Pay,
   Jail,
   Jail_free
};

class Card_Chance_Community: public QGraphicsItem{

public:

    Card_Chance_Community(const QString &content,const Card_category &category,std::vector<Card_type> &types);
    ~Card_Chance_Community();

    Card_category getCategory() const;
    QString getContent() const;
    std::vector<Card_type> get_card_types() const;
    void show(std::ostream &out) const;
    void display_card();

private:

    QString card_content_;
    Card_category card_category_;
    std::vector<Card_type> card_types_;
    QString Type() const;
    QString Category() const;

public:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};
std::ostream &operator<<(std::ostream &out, const Card_Chance_Community &cardcc);
#endif // CARD_CHANCE_COMMUNITY_HPP
