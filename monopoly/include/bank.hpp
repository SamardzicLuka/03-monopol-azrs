#ifndef BANK_HPP
#define BANK_HPP
#include "player.hpp"
#include "field.hpp"

#include <iostream>
#include <unordered_map>

#define __SALARY__ (200)

class Bank{

public:

    static Bank& return_bank(){
        static Bank bank {};

        return bank;
    }

    void give_init_money(Player& p);
    void pay_salary();
    void sell_property(const unsigned  id);
    void buy_property(const Field f);
    void loan_money(const int mortgage);
    bool sell_house(const int price);
    bool sell_hotel(const int price);
    void buy_estate(const int price, bool house);
    void collect(const int tax);
    void card_payment(const bool pays, const int sum);
    int get_budget();
    int get_house_counter();
    int get_hotel_counter();
    bool check_possesion(const unsigned id) const;
    void add_fields(std::vector<Field*> fields);
    void add_houses(unsigned const houses);
    void add_hotels(unsigned const houses);

private:
    int budget;
    int house_counter;
    int hotel_counter;
    std::unordered_map<unsigned, Field> properties;

    Bank() : budget (20580), house_counter(32), hotel_counter(12){};

    Bank(const Bank& bank ) = delete;
    Bank& operator=(const Bank& bank) = delete;
    
    void check_budget(const int sum);
    

};
#endif
