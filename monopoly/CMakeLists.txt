cmake_minimum_required(VERSION 3.16.0)

project(monopoly VERSION 1.0.0 LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)



set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC OFF)


find_package(Qt5Widgets REQUIRED)
find_package(Qt5Core REQUIRED)
find_package(Qt5Multimedia REQUIRED)


include_directories(include/)



set(SOURCES
    src/game_graphics.cpp
    src/game_logic.cpp
    src/game_over.cpp
    src/rules.cpp
    src/pause.cpp
    src/button.cpp
    src/game.cpp
    src/main.cpp
    src/bank.cpp
    src/card_chance_community.cpp
    src/card_property.cpp
    src/board.cpp
    src/field.cpp
    src/player.cpp
    src/bot.cpp
    src/notification.cpp
    src/animate_pixmap.cpp
    src/animate_player.cpp
    src/my_pixmap_graphics_item.cpp
    src/rectangle_with_text.cpp
    src/welcome.cpp
    src/keyPressEventFilter.cpp
    src/game_tax.cpp
    src/game_jail.cpp
    src/game_mortgage.cpp
    src/game_options.cpp
    )

set(UI_SRC
    forms/rules.ui
    forms/welcome.ui
    forms/game_over.ui
    forms/pause.ui
    )
qt5_wrap_ui(UI_HEADERS ${UI_SRC})
qt5_wrap_cpp(MOC_SRC
    include/button.hpp
    include/game.hpp
    include/rules.h
    include/pause.h
    include/animate_pixmap.hpp
    include/my_pixmap_graphics_item.hpp
    include/rectangle_with_text.hpp
    include/keyPressEventFilter.h
    include/board.hpp
    include/welcome.hpp
    include/animate_player.hpp
    include/game_over.h)

set(RSC resources.qrc)
add_executable(${PROJECT_NAME} ${SOURCES} ${UI_HEADERS} ${RSC} ${MOC_SRC})


target_link_libraries(${PROJECT_NAME} Qt5::Widgets Qt5::Core Qt5::Multimedia)
